import React from 'react'

const notFound = () => (
    <div className='notfound'>
        <h1 className='notfound__heading'>404 not found</h1>
        <p className='notfound__paragraph'>The link you requested does not exist</p>
    </div>
);

export default notFound;