import React from 'react';
import Navbar from '../components/Navbar.js';

const layout = (props) => (
    <div>
        <Navbar />
        {props.children}
    </div>
);

export default layout;